from math import floor
import numpy as np
import torch
import torch.nn as nn


class CustomModel(nn.Module):
    """Custom Model Modular instanciation"""

    def __init__(self, layers, input_shape):
        """ 
        Argsg:
            layers (array<sting>): Array containing the description of the successive layers
            input_shape (tuple): input shape WARNING images shapes should be structured as (Channels, Width, Height)
        """
        super().__init__()

        assert isinstance(layers, list)
        for layer in layers:
            assert isinstance(layer, str)
        self.layers = layers
        self.input_shape = tuple(input_shape)
        self.shapes = [tuple(input_shape)]

        for i, layer_str in enumerate(layers):
            params = layer_str.split("_")
            layer_type, params = params[0], params[1:]
            if not layer_type in [
                "dense",
                "conv2d",
                "maxpool2d",
                "flatten",
                "batchnorm1d",
                "batchnorm2d",
                "dropout",
                "relu",
                "softmax",
            ]:
                print(layer_type)
                raise AssertionError()

            if layer_type == "dense":
                in_shape = self.shapes[-1]
                assert len(in_shape) == 1
                n_neurons = int(params[0])
                layer = nn.Linear(in_shape[0], n_neurons)
                self.shapes.append((n_neurons,))

            elif layer_type == "conv2d":
                in_shape = self.shapes[-1]
                assert len(in_shape) == 3 or len(in_shape) == 2
                in_channels, in_width, in_height = (
                    in_shape if len(in_shape) == 3 else (1,) + in_shape
                )
                n_filters, kernel_size, stride = (
                    int(params[0]),
                    int(params[1]),
                    int(params[2]),
                )
                padding = int(params[3]) if len(params) > 3 else 1
                layer = nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=n_filters,
                    kernel_size=kernel_size,
                    stride=stride,
                    padding=padding,
                )
                self.shapes.append(
                    (
                        n_filters,
                        floor((in_width + 2 * padding - kernel_size) / stride) + 1,
                        floor((in_height + 2 * padding - kernel_size) / stride) + 1,
                    )
                )
            elif layer_type == "flatten":
                layer = nn.Flatten()
                in_shape = self.shapes[-1]
                out_shape = np.prod(in_shape)
                self.shapes.append((out_shape,))

            elif layer_type == "maxpool2d":
                in_shape = self.shapes[-1]
                assert len(in_shape) == 3
                in_channels, in_width, in_height = in_shape
                kernel_size = int(params[0])
                stride = int(params[1]) if len(params) > 1 else kernel_size
                padding = int(params[2]) if len(params) > 2 else 0
                layer = nn.MaxPool2d(
                    kernel_size=kernel_size, stride=stride, padding=padding
                )
                self.shapes.append(
                    (
                        in_channels,
                        floor((in_width + 2 * padding - kernel_size) / stride) + 1,
                        floor((in_height + 2 * padding - kernel_size) / stride) + 1,
                    )
                )
            elif layer_type == "batchnorm1d":
                in_shape = self.shapes[-1]
                layer = nn.BatchNorm1d(in_shape[0])
                self.shapes.append(in_shape)

            elif layer_type == "batchnorm2d":
                in_shape = self.shapes[-1]
                layer = nn.BatchNorm2d(in_shape[0])
                self.shapes.append(in_shape)

            elif layer_type == "dropout":
                in_shape = self.shapes[-1]
                drop_rate = float(params[0])
                layer = nn.Dropout(drop_rate)
                self.shapes.append(in_shape)

            elif layer_type == "relu":
                in_shape = self.shapes[-1]
                layer = nn.ReLU()
                self.shapes.append(in_shape)

            elif layer_type == "softmax":
                dimension = int(params[0])
                in_shape = self.shapes[-1]
                layer = nn.Softmax(dim=dimension)
                self.shapes.append(in_shape)

            setattr(self, "hidden_{}".format(i), layer)

    def forward(self, x):
        for i in range(len(self.layers)):
            layer = getattr(self, "hidden_{}".format(i))
            x = layer(x)
        return x

    def print_shapes(self):
        for descr, shape in zip(["Input"] + self.layers, self.shapes):
            line = "--> {} --> :".format(descr)
            line += " " * (25 - len(line))
            line += str(shape)
            print(line)


class ConvNet2(nn.Module):
    def __init__(self):
        super(ConvNet2, self).__init__()

        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(2, 2)
        self.drop1 = nn.Dropout(0.3)
        self.drop2 = nn.Dropout(0.5)
        self.drop3 = nn.Dropout(0.25)

        ## CNN 1
        self.conv1 = nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1)
        self.batchnorm1 = nn.BatchNorm2d(32, affine=False)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1)

        ## CNN 2
        self.conv3 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.batchnorm2 = nn.BatchNorm2d(64, affine=False)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)

        ## CNN 3
        self.conv5 = nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1)
        self.batchnorm3 = nn.BatchNorm2d(128, affine=False)
        self.conv6 = nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1)

        ## Output
        self.flat = nn.Flatten()

        self.fc1 = nn.Linear(8192, 512)
        self.batchnorm4 = nn.BatchNorm1d(512, affine=False)

        self.fc2 = nn.Linear(512, 128)
        self.fc3 = nn.Linear(128, 10)

        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):

        ## CNN 1
        x = self.relu(self.conv1(x))
        x = self.batchnorm1(x)

        x = self.relu(self.conv2(x))
        x = self.batchnorm1(x)

        x = self.pool(x)
        x = self.drop1(x)

        ## CNN 2
        x = self.relu(self.conv3(x))
        x = self.batchnorm2(x)

        x = self.relu(self.conv4(x))
        x = self.batchnorm2(x)

        x = self.pool(x)
        x = self.drop1(x)

        ## CNN 3
        x = self.relu(self.conv5(x))
        x = self.batchnorm3(x)

        x = self.relu(self.conv6(x))
        x = self.batchnorm3(x)

        x = self.pool(x)
        x = self.drop2(x)

        ## OUTPUT
        x = self.flat(x)

        x = self.relu(self.fc1(x))

        x = self.batchnorm4(x)

        x = self.drop2(x)

        x = self.relu(self.fc2(x))
        x = self.drop3(x)

        x = self.softmax(self.fc3(x))

        return x

