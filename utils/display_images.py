from os import path
import cv2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from torchvision import utils
from math import ceil


def browse_images(refs_df, folder_path, start_index=0):
    """A fonction opening a window aside to browse amont a folder containing images
    Args:
        refs_df (Dataframe): Dataframe with columns ['img', 'classname', 'subject'] where img should contain all the file names
        data_folder (string): Path of folder which contains the images
        start_index (int): Start index of the browsing (default = 0)
    """

    print(
        "--- Browsing Program Started (See an openCV windows open aside on your desktop) ---\n'n': Next\t'p': Previous\t'q': Quit"
    )

    number_of_images = len(refs_df)
    if refs_df is None or number_of_images == 0:
        print("error no images")
        return

    i = min(number_of_images - 1, max(start_index, 0))
    while True:
        df_row = refs_df.iloc[i]
        image = cv2.imread(path.join(folder_path, df_row["img"]))
        image_name = "Image[{}] - {}  ({}) ({})".format(
            i, df_row["classname"], df_row["subject"], df_row["img"]
        )
        cv2.namedWindow(image_name)
        cv2.moveWindow(image_name, 1000, 500)
        cv2.imshow(image_name, image)

        key = cv2.waitKey(0)

        if key == ord("n") and i < number_of_images - 1:
            i += 1
        elif key == ord("p") and i > 0:
            i -= 1
        elif key == ord("q"):
            break
        cv2.destroyAllWindows()
        i = max(0, i)
        i = min(number_of_images - 1, i)
        cv2.destroyAllWindows()

    cv2.destroyAllWindows()
    print("--- Browsing Program Ended ---")


def show_images_batch(sample_batched, targets=None):
    """Show image for a batch of samples.
    Args:
        sample_batched (torchBatch)
    """
    images_batch, labels, subjects, target = (
        sample_batched["image"].float(),
        sample_batched["classname"],
        sample_batched["subject"],
        sample_batched["target"],
    )
    batch_size = len(images_batch)

    if len(images_batch.shape) == 3:
        images_batch.unsqueeze_(1)
    grid = utils.make_grid(images_batch, padding=10, nrow=min(images_batch.shape[0], 3))
    plt.figure(figsize=(18, 12))
    plt.imshow(grid.numpy().transpose((1, 2, 0)))
    plt.title("Batch from dataloader")
    plt.axis("off")
    plt.ioff()
    plt.show()
