from skimage import transform
import numpy as np
import cv2
import torch

class TransformRescale(object):
    """Rescale the image in a sample to a given size."""

    def __init__(self, output_size):
        """
        Args:
            output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
        """
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        image, classname, subject = sample['image'], sample['classname'], sample['subject']
        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        image = transform.resize(image, (new_h, new_w))
        return {**sample, 'image': image}
    

class TransformRandomCrop(object):
    """Crop randomly the image in a sample."""

    def __init__(self, output_size):
        """
        Args:
            output_size (tuple or int): Desired output size. If int, square crop is made.
        """
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image, classname, subject = sample['image'], sample['classname'], sample['subject']

        h, w = image.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image = image[top: top + new_h, left: left + new_w]

        return {**sample, 'image': image}

class TransformGreyScale(object):
    """ Transforms images to grey_scale"""
    def __call__(self, sample):
        image, classname, subject = sample['image'], sample['classname'], sample['subject']
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
        return {**sample, 'image': image}






class TransformToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, classname, subject = sample['image'], sample['classname'], sample['subject']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W WARNING : It causes troubles with matplotlib.pyplot.imshow(image) 
        # which expect a numpy-like image format
        if(len(image.shape) == 3):
            image = image.transpose((2, 0, 1))
            
        return {**sample, 'image': torch.from_numpy(image)}