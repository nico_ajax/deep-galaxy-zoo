import torch
from torch.utils.data import Dataset
from os import listdir
from os.path import join, isfile
import cv2
import pandas as pd
import numpy as np
import random
from math import floor


def load_data_references(csv_file, data_folder):
    """Load References of the data used to load and manipulate dataset
    Args:
        csv_file (string): Path to the csv file containing info for each file name
        data_folder (string): Path of the data folder used to remove non-matching references from csv_file
    """

    df_refs = pd.read_csv(csv_file)
    filenames = [f for f in listdir(data_folder) if isfile(join(data_folder, f))]
    df_refs = df_refs[df_refs["img"].isin(filenames)]

    labels_map = {
        "c0": 0,
        "c1": 1,
        "c2": 2,
        "c3": 3,
        "c4": 4,
        "c5": 5,
        "c6": 6,
        "c7": 7,
        "c8": 8,
        "c9": 9,
    }
    df_refs["target"] = df_refs["classname"].replace(labels_map)
    return df_refs


def split_refs_df(df_dataset, split_fraction=0.7, random_state=42):
    """Split References of the data between a Train set and a Test set but keep the proportion of each class in the dataset
    Args:
        df_dataset (Dataframe): Dataframe containing all references of the data you want to use (train + test)
        split_fraction (int): Proportion of first data in the split 
        RANDOM_STATE (int): seed used to draw samples randomly
    Return:
        train_set (Dataframe): References to the data of the train set
        test_set (Dataframe): References to the data of the test set
    """
    df_train_set = pd.DataFrame(columns=df_dataset.columns)
    df_test_set = pd.DataFrame(columns=df_dataset.columns)

    classnames = df_dataset["classname"].unique()
    for classname in classnames:
        dataset_classname = df_dataset[
            df_dataset["classname"] == classname
        ].reset_index(drop=True)
        train_set_classname = dataset_classname.sample(
            frac=split_fraction, replace=False, random_state=random_state
        )
        test_set_classname = dataset_classname.iloc[
            list(set(dataset_classname.index) - set(train_set_classname.index))
        ]

        df_train_set = df_train_set.append(train_set_classname, ignore_index=False)
        df_test_set = df_test_set.append(test_set_classname, ignore_index=False)
    return df_train_set, df_test_set


def split_refs_df_by_subject(df_dataset, split_fraction=0.7, random_state=42):
    df_train_set = pd.DataFrame(columns=df_dataset.columns)
    df_test_set = pd.DataFrame(columns=df_dataset.columns)

    subjects = df_dataset["subject"].unique()

    training_subjects = np.random.choice(
        subjects, floor(split_fraction * len(subjects)), replace=False
    )
    df_train_set = df_dataset[
        df_dataset["subject"].isin(training_subjects)
    ].reset_index(drop=True)
    df_test_set = df_dataset[
        ~df_dataset["subject"].isin(training_subjects)
    ].reset_index(drop=True)
    return df_train_set, df_test_set


class DriversDataset(Dataset):
    """Drivers dataset."""

    def __init__(self, refs_df, images_dir, transform=None):
        """
        Args:
            refs_df (Dataframe): Dataframe of the references to the images with columns ['img', 'classname', 'target', 'subject'] where img should contain all the file names.
            images_dir (string): Path of the images folder.
            transform (function): Optional transform to be applied on a sample.
        """
        self.refs = refs_df
        self.images_dir = images_dir
        self.transform = transform
        # print('--- Pytorch Dataset of {} images ready to be used. ---'.format(len(self.refs)))

    def __len__(self):
        return len(self.refs)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        df_row = self.refs.iloc[idx]
        image_name = join(self.images_dir, df_row["img"])
        image = cv2.imread(image_name)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        sample = {
            "image": image,
            "classname": df_row["classname"],
            "target": df_row["target"],
            "subject": df_row["subject"],
        }

        if self.transform:
            sample = self.transform(sample)

        return sample

