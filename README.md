# Developping a deep learning model to identify if a driver is paying attention to his surroundings or is being distracted of the surroundings.

# Repository expected structure

- data/
  - imgs/
    - train/ : contains all the images directly
    - test/
  - sample_submission.csv/
  - driver_imgs_list.csv
- utils/ : contains all the python modules we use to build/train/evaluate models
- notebooks/ : contains the notebooks